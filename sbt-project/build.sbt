name := "board"

version := "1.1"

scalaVersion := "2.10.1"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.1.3" % "test"

libraryDependencies += "com.novocode" % "junit-interface" % "0.10-M2" % "test"
