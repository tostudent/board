package testBoard1

import board2.Main
import board2.Main.Board
import org.scalatest.FunSuite

class SetSuite extends FunSuite {

  val board = new Board

  test("Left X-coordinate = (0)") {
    assert(!board.inBounds(-1,0) && board.inBounds(0,0))
  }

  test("Right X-coordinate = (7)") {
    assert(!board.inBounds(8,0) && board.inBounds(7,0))
  }

  test("Up Y-coordinate = (0)") {
    assert(!board.inBounds(0,-1) && board.inBounds(0,0))
  }

  test("Down Y-coordinate = (7)") {
    assert(!board.inBounds(0,8) && board.inBounds(0,7))
  }

  //---

  test("Coordinate (-1,-1) outside the board") {
    assert(!board.inBounds(-1,-1))
  }

  test("Coordinate (8,8) outside the board") {
    assert(!board.inBounds(8,8))
  }

  //---

  test("Outside the board (-1,-1) GET = -1") {
    assert(board.get(-1,-1) == -1)
  }

  test("Outside the board (8,8) GET = -1") {
    assert(board.get(8,8) == -1)
  }

  test("Inside the board GET >=0 && <=7") {
    for (y <- 0 until board.bsize; x <- 0 until board.bsize) {
      val value = board.get(x,y)
      assert(value >= 0 && value <=7)
    }
  }

  test("Set == Get") {
    for (y <- 0 until board.bsize; x <- 0 until board.bsize) {
      board.set(x,y,3)
      assert(board.get(x,y) == 3)
    }
  }

  //---

  test("There is move in new board") {
    val board = new Board
    assert(board.advice.to.x != -1)
  }


  test("No pairs in new board (check 100 times)") {
    for (n <- 1 to 100) {
      val board = new Board

      for(y <- 0 until board.bsize; x <- 0 until board.bsize-2)
        assert(board.get(x,y) != board.get(x+1,y))

      for(x <- 0 until board.bsize; y <- 0 until board.bsize-2)
        assert(board.get(x,y) != board.get(x,y+1))
    }
  }

  //---

  def fillBoardSerial {
    for (y <- 0 until board.bsize; x <- 0 until board.bsize)
      board.set(x,y,y*10+x+1)
  }


  test("Horizontal advice1") {
    fillBoardSerial

    board.set(3,2,0)
    board.set(4,2,0)
    board.set(6,2,0)
    val adv = board.advice
    assert(adv.to.x == 5 && adv.to.y == 2 && adv.from.x == 6 && adv.from.y == 2)
  }

  test("Horizontal advice2") {
    fillBoardSerial

    board.set(3,2,0)
    board.set(4,2,0)
    board.set(5,1,0)
    val adv = board.advice
    assert(adv.to.x == 5 && adv.to.y == 2 && adv.from.x == 5 && adv.from.y == 1)
  }

  test("Horizontal advice3") {
    fillBoardSerial

    board.set(3,2,0)
    board.set(4,2,0)
    board.set(5,3,0)
    val adv = board.advice
    assert(adv.to.x == 5 && adv.to.y == 2 && adv.from.x == 5 && adv.from.y == 3)
  }

  test("Horizontal advice4") {
    fillBoardSerial

    board.set(3,2,0)
    board.set(4,1,0)
    board.set(5,2,0)
    val adv = board.advice
    assert(adv.to.x == 4 && adv.to.y == 2 && adv.from.x == 4 && adv.from.y == 1)
  }

  test("Horizontal advice5") {
    fillBoardSerial

    board.set(3,2,0)
    board.set(4,3,0)
    board.set(5,2,0)
    val adv = board.advice
    assert(adv.to.x == 4 && adv.to.y == 2 && adv.from.x == 4 && adv.from.y == 3)
  }

  //---





}

 
