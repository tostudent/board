package board2

import java.util.Random

object Main {

  case class Point(x:Int, y:Int)
  case class ToFrom(to:Point, from:Point)

  class Board {
    val bsize = 8
    val bcolors = 6

    val board = new Array[Array[Int]](bsize)

    for (y <- 0 until bsize)
      board(y) = new Array[Int](bsize)
    //**********************************

    fillWithMove


    def clearBoard {
      for (y <- 0 until bsize; x <- 0 until bsize)
          set(x,y,-1)
    }

    def fillOnlyEmptyCells {
      for (y <- 0 until bsize; x <- 0 until bsize)
        if (get(x,y) == -1) set(x,y,newRnd(x,y))
    }

    def fillWithMove {
      do {
        clearBoard
        fillOnlyEmptyCells
      } while (advice.to.x == -1)
    }

    //**********************************


    def get(x:Int, y:Int) = {
      if (inBounds(x,y))
        board(y)(x)
      else
         -1
    }

    def set(x:Int, y:Int, value:Int) = {
      board(y)(x)=value
    }

    def inBounds(x:Int, y:Int):Boolean = {
      (x > -1) && (x < bsize) && (y > -1) && (y < bsize)
    }

    //==================================

    def newRnd(x:Int, y:Int) = {
      val rndarr = 0 until bcolors

      val clrrndarr = rndarr.filter(z => z!=get(x-1,y) && z!=get(x+1,y) && z!=get(x,y-1) && z!=get(x,y+1))

      clrrndarr(new Random().nextInt(clrrndarr.length))
    }

    //==================================

    def advice:ToFrom = {
      for (y <- 0 until bsize; x <- 0 to bsize-3) {
        val value = get(x,y)

        if (get(x+1,y) == value) {
          if (get(x+3,y) == value)
             return ToFrom(Point(x+2,y),Point(x+3,y))

          if (get(x+2,y-1) == value)
             return ToFrom(Point(x+2,y),Point(x+2,y-1))

          if (get(x+2,y+1) == value)
             return ToFrom(Point(x+2,y),Point(x+2,y+1))
        }


        if (get(x+2,y) == value) {
          if (get(x+1,y-1) == value)
           return ToFrom(Point(x+1,y),Point(x+1,y-1))

          if (get(x+1,y+1) == value)
           return ToFrom(Point(x+1,y),Point(x+1,y+1))
        }
      }

      //---

      for (x <- 0 until bsize; y <- 0 to bsize-3) {
        val value = get(x,y)

        if (get(x,y+1) == value) {
          if (get(x,y+3) == value)
            return ToFrom(Point(x,y+2),Point(x,y+3))

          if (get(x-1,y+2) == value)
            return ToFrom(Point(x,y+2),Point(x-1,y+2))

          if (get(x+1,y+2) == value)
            return ToFrom(Point(x,y+2),Point(x+1,y+2))
        }


        if (get(x,y+2) == value) {
          if (get(x-1,y+1) == value)
           return ToFrom(Point(x,y+1),Point(x-1,y+1))

          if (get(x+1,y+1) == value)
           return ToFrom(Point(x,y+1),Point(x+1,y+1))
        }
      }

      return ToFrom(Point(-1,0),Point(0,0))
    }

    //==================================

    def printBoard {
      for (y <- 0 until bsize) {
        for(x <- 0 until bsize) {
          val value = get(x,y)

          if (value >= 0)
            print(" " + value)
          else
            print(" *")
        }

        Console.println
      }

      Console.println("-----------------\n")
    }

  }


  //---------------------------------------------------------------------


  def main(args: Array[String]) = {

    Console.print("\nStarting...\n\n")

    val m3 = new Board

    Console.print("\nNew board:\n\n")
    m3.printBoard

    val adv = m3.advice
    Console.println("Advice:")
    Console.println("x:"+adv.from.x+", y:"+adv.from.y+" -> x:"+adv.to.x+", y:"+adv.to.y+"\n")

    val v0 = m3.get(adv.to.x, adv.to.y)
    val v1 = m3.get(adv.from.x, adv.from.y)

    m3.set(adv.to.x, adv.to.y, v1)
    m3.set(adv.from.x, adv.from.y, v0)

    m3.printBoard
  }

}
 
